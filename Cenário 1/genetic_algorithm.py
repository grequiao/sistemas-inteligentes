# -*- coding: utf-8 -*-

import numpy as np
import math
import copy

# =============================================================================
# costs = np.array([[0,29,82,46,68,52,72,42,51,55,29,74,23,72,46],
#                    [29,0,55,46,42,43,43,23,23,31,41,51,11,52,21],
#                    [82,55,0,68,46,55,23,43,41,29,79,21,64,31,51],
#                    [46,46,68,0,82,15,72,31,62,42,21,51,51,43,64],
#                    [68,42,46,82,0,74,23,52,21,46,82,58,46,65,23],
#                    [52,43,55,15,74,0,61,23,55,31,33,37,51,29,59],
#                    [72,43,23,72,23,61,0,42,23,31,77,37,51,46,33],
#                    [42,23,43,31,52,23,42,0,33,15,37,33,33,31,37],
#                    [51,23,41,62,21,55,23,33,0,29,62,46,29,51,11],
#                    [55,31,29,42,46,31,31,15,29,0,51,21,41,23,37],
#                    [29,41,79,21,82,33,77,37,62,51,0,65,42,59,61],
#                    [74,51,21,51,58,37,37,33,46,21,65,0,61,11,55],
#                    [23,11,64,51,46,51,51,33,29,41,42,61,0,62,23],
#                    [72,52,31,43,65,29,46,31,51,23,59,11,62,0,59],
#                    [46,21,51,64,23,59,33,37,11,37,61,55,23,59,0]])
# =============================================================================


local = [('PAVILHÃO DE EVENTOS DO PARQUE BARIGUI', (-25.427598506844276, -49.313545333132865)),
         ('US VILA DIANA', (-25.367796503239884, -49.268788602277674)),
         ('US BOM PASTOR', (-25.413334761300813, -49.301304473440794)),
         ('US SANTA QUITÉRIA I', (-25.461662948370122, -49.316744175291504)),
         ('US FANNY-LINDOIA', (-25.478518790104918, -49.27270853111126)),
         ('US WALDEMAR MONASTIER', (-25.497324865786624, -49.22764136361357)),
         ('US CAJURU', (-25.45219610841501, -49.217564515768025)),
         ('US SÃO MIGUEL', (-25.480207557348237, -49.33666295809527)),
         ('US SÃO JOÃO DEL REY', (-25.537571661106217, -49.273294902273946)),
         ('US MORADIAS SANTA RITA', (-25.560796147378106, -49.33651793110936))]

SIZE = len(local)

def measure(coord1, coord2):
    R = 6378.137
    dLat = coord2[0] * math.pi / 180 - coord1[0] * math.pi / 180
    dLon = coord2[1] * math.pi / 180 - coord1[1] * math.pi / 180
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(coord1[0] * math.pi / 180) * math.cos(coord2[0] * math.pi / 180) * math.sin(dLon/2) * math.sin(dLon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = R * c
    return d * 1000

def convertMatrix(convert):
    matrix = np.zeros((SIZE, SIZE))
    for i1, l1 in enumerate(local):
        for i2, l2 in enumerate(local):
            if i1 == i2:
                matrix[i1][i2] = 0
            else:
                matrix[i1][i2] = measure(l1[1], l2[1])
    return matrix


def fn_cost(obj):
    #cost = 0
    #for i in range(14):
    #    cost += costs[obj.index(i)][obj.index(i+1)]
    #return cost
    cost = 0
    for i in range(SIZE - 1):
        cost += costs[obj[i]][obj[i+1]]
    return cost

def fn_fitness(obj):
    max_value = 0
    for i in obj:
        if i["cost"] > max_value:
            max_value = i["cost"]
    for i in obj:
        i["fitness"] = max_value - i["cost"] + 1
    
def prob(obj):
    total = 0
    for i in obj:
        total += i["fitness"]
    for i in obj:
        i["prob"] = i["fitness"] / total

def convert(obj):
    abj = []
    for i in range(len(obj)):
        abj.append(obj.index(i))
    return abj

def crossover(p1, p2):
    p1 = list(p1)
    p2 = list(p2)
    child = [copy.deepcopy(p1), copy.deepcopy(p2)]
    
    rand = np.random.randint(0, SIZE, size = 2)
    while rand[0] == rand[1]:
        rand = np.random.randint(0, SIZE, size = 2)
    rand = np.sort(rand)
    i, j = rand[0], rand[1]
    
    for x in range(i, j):
        #new[x], new[new.index(p2[x])] = p2[x], new[x]
        a, b = x, child[0].index(p2[x])
        child[0][a], child[0][b] = child[0][b], child[0][a]
    for x in [y for y in range(0, len(p1)) if y not in list(range(i, j))]:
        #new[x], new[new.index(p2[x])] = p2[x], new[x]
        a, b = x, child[1].index(p1[x])
        child[1][a], child[1][b] = child[1][b], child[1][a]
    return child

def mutate(child, mutation_chance):
    while(np.random.random() <= mutation_chance):
        swap = np.random.randint(0, SIZE, size = 2)
        while(swap[0] == swap[1]):
            swap = np.random.randint(0, SIZE, size = 2)
        a, b = swap[0], swap[1]
        child[a], child[b] = child[b], child[a]

def roulette(obj):
    n = np.random.random()
    selected = None;
    si = 0
    for count, i in enumerate(obj):
        si += i["prob"]
        if si >= n:
            selected = count
            break;
    return selected
    

def main():
    mutation_chance = 0.05
    pop = 100
    iterations = 100
    generation = 0
    
    population = []
    
    for i in range(pop):
        rand = np.array(range(SIZE))
        np.random.shuffle(rand)
        
        population.append({"cost":fn_cost(rand), "genome":rand, "fitness":0, "prob":0})
    
    
    while(generation < iterations):
        fn_fitness(population)
        prob(population)
        
        for i in range(pop):
            p1 = roulette(population)
            p2 = roulette(population)
            while p1 == p2:
                p2 = roulette(population)
            child = crossover(population[p1]["genome"], population[p2]["genome"])
            for j in child:
                mutate(j, mutation_chance)
                new = {"cost":fn_cost(j), "genome":j, "fitness":0, "prob":0}
                population.append(new)
        fn_fitness(population)
        population = sorted(population, key = lambda n: n["cost"])
        del population[pop:]
        
        generation += 1
    print(population[0])
 
    

if __name__== "__main__":
    costs = convertMatrix(local)
    main()
